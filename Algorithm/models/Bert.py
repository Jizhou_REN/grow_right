from transformers import DistilBertTokenizer, DistilBertForSequenceClassification, DistilBertModel, DistilBertTokenizerFast
import torch
import numpy as np
from torch import nn
import torch.optim as optim
from sklearn import metrics
from sklearn.metrics import f1_score
from transformers import BertTokenizer, BertModel, BertConfig, AdamW

class Config(object):
    def __init__(self):
        self.job_description = './dataset/seek_australia.csv'
        self.education_path = './dataset/education.csv'
        self.skill_path = './dataset/skill.csv'
        self.interest_path = './dataset/interest.csv'
        self.user_data = './dataset/selected_user_features.csv'
        self.merge_job_path = './dataset/merge_job.csv'

        self.user_feautres = ['ID',
            'Degree1', 'Degree2', 'Field of Study1','Interest_1',
            'Interest_2','Interest_3', 'Interest_4', 'Interest_5', 'Interest_6', 'Skill'
                       ]
        self.job_features = ['url','category', 'city', 'company_name'
                             , 'job_description', 'job_title',
                             'salary_offered', 'state']


        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')
        self.model_name = 'distilbert-base-uncased'
        self.max_length = 128
        self.batch_size = 1
        self.lr = 1e-5
        self.num_epochs = 10
        self.weight_deacy = 1e-2