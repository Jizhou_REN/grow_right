import pandas as pd
import torch
import pandas as pd
from torch.utils.data import Dataset, DataLoader, random_split
from importlib import import_module
import torch
from torch.utils.data import Dataset, DataLoader
from transformers import AlbertForSequenceClassification, AlbertTokenizer, AdamW, get_linear_schedule_with_warmup
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import os
from sklearn.metrics import f1_score, confusion_matrix
import warnings
def read_csv(config):
    """

    :param config:
    :return: job_descs: id, job_desc, link, title, category
    user_resumes: candidate, job_title, user_resume, hash_id
    labels: job_id, user_hash, label
    """
    job_descs = pd.read_csv(config.job_descriptions, encoding='utf-8')
    user_resumes = pd.read_csv(config.user_resumes,encoding='utf-8')
    labels = pd.read_csv(config.labels, encoding='utf-8')
    return job_descs, user_resumes, labels


def construct_data(job_descs,user_resumes,labels):
    """
    constracut dataset: user resume, job desc, label
    :param job_descs:
    :param user_resumes:
    :param labels:
    :return: DataFrame
    """
    match_user_ids = labels['candidate_hash'].tolist()
    match_user_ids = [str(idx) for idx in match_user_ids]
    user_resumes.set_index('hash_id', inplace=True)
    index_df = pd.DataFrame({'index': match_user_ids})
    user_resumes.drop_duplicates(inplace=True)

    result = pd.merge(index_df, user_resumes, left_on='index', right_index=True, how='left', validate="many_to_one")
    job_descs.set_index('ID', inplace=True)

    match_job_ids = labels['job_id'].tolist()
    match_job_ids = [str(idx) for idx in match_job_ids]
    index_df2 = pd.DataFrame({'index': match_job_ids})
    result2 = pd.merge(index_df2, job_descs, left_on='index', right_index=True, how='left')
    result2.drop_duplicates()
    relevance_labels = labels['relevance']
    data = pd.concat([result['description'], result2['Job Description'], relevance_labels], axis=1)
    data.columns=['user resumes', 'job descriptions', 'labels']
    data = data.dropna(subset=['labels'])
    data = data.drop(data[(data['labels'] != 0.0) & (data['labels'] != 1.0)].index)
    data['labels'] = data['labels'].astype(int)
    data.dropna(inplace=True)
    data.replace('', pd.np.nan, inplace=True)
    data.replace(r'^\s*$', pd.np.nan, regex=True, inplace=True)
    data.dropna(subset=['user resumes', 'job descriptions'],  how='any', inplace=True)

    data.to_csv('./dataset/labeled_dataset.csv', index=False)

    return data


class JobRecommendationDataset(Dataset):
    def __init__(self, config, data, tokenizer):
        self.data = data
        self.tokenizer = tokenizer
        self.max_length = config.max_length

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        row = self.data.iloc[idx]
        user_resume, job_description, label = row['user resumes'], row['job descriptions'], row['labels']
        inputs = self.tokenizer.encode_plus(
            user_resume,
            job_description,
            add_special_tokens=True,
            padding=False,
            truncation=False,
            return_tensors='pt',
            return_token_type_ids=True,
            return_attention_mask=True
        )

        input_ids = inputs["input_ids"].squeeze()
        attention_mask = inputs["attention_mask"].squeeze()
        token_type_ids = inputs["token_type_ids"].squeeze()

        max_length = 128
        if len(input_ids) > max_length:
            input_ids = input_ids[:max_length]
            token_type_ids = token_type_ids[:max_length]
            attention_mask = attention_mask[:max_length]
        else:
            padding_length = max_length - len(input_ids)
            input_ids = torch.cat([input_ids, torch.zeros(padding_length, dtype=torch.long)], dim=0)
            token_type_ids = torch.cat([token_type_ids, torch.zeros(padding_length, dtype=torch.long)], dim=0)
            attention_mask = torch.cat([attention_mask, torch.zeros(padding_length, dtype=torch.long)], dim=0)

        #
        return {
            "input_ids": input_ids,
            "token_type_ids": token_type_ids,
            "attention_mask": attention_mask,
            "labels": torch.tensor(label, dtype=torch.long)
        }
def split_dataset(data):
    train_df,test_df = train_test_split(data, test_size=0.2)

    train_df.to_csv('train.csv', index=False)
    test_df.to_csv('test.csv', index=False)
    return train_df, test_df


def train(config, model, dataloader, optimizer, scheduler):
    model.train()
    total_loss = 0
    all_preds = []
    all_labels = []
    sample_losses = []
    for batch in dataloader:
        optimizer.zero_grad()
        input_ids, token_type_ids, attention_mask, labels = [x.to(config.device) for x in batch.values()]
        outputs = model(input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask, labels=labels)
        loss = outputs.loss
        total_loss += loss.item()

        logits = outputs.logits
        preds = torch.argmax(logits, dim=1).detach().cpu().numpy()
        all_preds.extend(preds)
        all_labels.extend(labels.detach().cpu().numpy())

        loss.backward()
        optimizer.step()
        scheduler.step()
    f1 = f1_score(all_labels, all_preds, average='weighted')
    confusion_mat = confusion_matrix(all_labels, all_preds)
    return total_loss / len(dataloader), f1, confusion_mat

import torch
import torch.nn as nn
import torch.nn.functional as F


class FocalLoss(nn.Module):
    def __init__(self, alpha, gamma=2):
        super(FocalLoss, self).__init__()
        self.alpha = torch.tensor(alpha).to('cuda')
        self.gamma = gamma

    def forward(self, inputs, targets):
        BCE_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduction='none')
        targets = targets.type(torch.long)
        expanded_alpha = self.alpha.expand_as(targets)
        at = expanded_alpha.gather(1, targets.data)
        pt = torch.exp(-BCE_loss)
        F_loss = at * (1 - pt) ** self.gamma * BCE_loss
        return F_loss.mean()


def train_2(config, model, dataloader, optimizer, scheduler):
    model.train()
    total_loss = 0
    all_preds = []
    all_labels = []
    sample_losses = []
    total_samples = 1297 + 337
    weight_0 = 337 / total_samples
    weight_1 = 1297 / total_samples
    alpha = [weight_0, weight_1]
    criterion = FocalLoss(alpha, gamma=2)
    for batch in dataloader:
        optimizer.zero_grad()
        input_ids, token_type_ids, attention_mask, labels = [x.to(config.device) for x in batch.values()]
        outputs = model(input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask, labels=labels)
        one_hot_targets = torch.nn.functional.one_hot(labels, num_classes=2).float()
        loss = criterion(outputs.logits, one_hot_targets)
        total_loss += loss.item()

        logits = outputs.logits
        preds = torch.argmax(logits, dim=1).detach().cpu().numpy()
        all_preds.extend(preds)
        all_labels.extend(labels.detach().cpu().numpy())

        loss.backward()
        optimizer.step()
        scheduler.step()
    f1 = f1_score(all_labels, all_preds, average='weighted')
    confusion_mat = confusion_matrix(all_labels, all_preds)
    return total_loss / len(dataloader), f1, confusion_mat

def evaluate(config, model, dataloader, optimizer):
    model.eval()

    total_loss = 0
    total_f1 = 0
    num_samples = 0

    with torch.no_grad():
        for batch in dataloader:
            batch = {k: v.to(config.device) for k, v in batch.items()}
            outputs = model(**batch)
            loss = outputs.loss
            preds = torch.argmax(outputs.logits, dim=-1)
            f1 = f1_score(batch["labels"].cpu(), preds.cpu(), average="weighted")

            total_loss += loss.item()
            total_f1 += f1 * batch["labels"].size(0)
            num_samples += batch["labels"].size(0)

    return total_loss / num_samples, total_f1 / num_samples

def recommend(config, model, tokenizer, user_resume, job_descriptions):
    model.eval()

    matched_jobs = []
    with torch.no_grad():
        for job_description in job_descriptions:
            warnings.filterwarnings("ignore")
            warnings.filterwarnings("ignore", message="Be aware, overflowing tokens are not returned.*")
            warnings.filterwarnings("ignore",
                                    message="Be aware, overflowing tokens are not returned for the setting you have chosen*")
            inputs = tokenizer.encode_plus(user_resume, job_description, return_tensors="pt", max_length=128, padding="max_length", truncation=True)
            input_ids = inputs["input_ids"].to(config.device)
            token_type_ids = inputs["token_type_ids"].to(config.device)
            attention_mask = inputs["attention_mask"].to(config.device)

            outputs = model(input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask)
            probs = torch.softmax(outputs.logits, dim=-1)
            if torch.argmax(probs) == 1:
                matched_jobs.append(job_description)

    return matched_jobs

if __name__ == '__main__':
    x = import_module('models.' + 'Albert')
    config = x.Config()
    job_descs, user_resumes, labels = read_csv(config)
    print(job_descs.head(1))
    print(user_resumes.head(1))
    print(labels.head(1))
    model = AlbertForSequenceClassification.from_pretrained('albert-base-v2', num_labels=2)
    tokenizer = AlbertTokenizer.from_pretrained('albert-base-v2')
    data = construct_data(job_descs, user_resumes, labels)
    dataset = JobRecommendationDataset(config, data,tokenizer)