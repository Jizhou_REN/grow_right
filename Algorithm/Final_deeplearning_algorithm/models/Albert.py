import torch

class Config(object):
    def __init__(self):

        self.job_descriptions = './dataset/job_description_list.csv'
        self.user_resumes = './dataset/employment_to_be_labelled.csv'
        self.labels = './dataset/job_description_relevance_label.csv'
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        # self.tokenizer = AutoTokenizer.from_pretrained(self.model_name)

        self.max_length = 128
        self.batch_size = 8
        self.lr = 1e-5
        self.num_epochs = 30
        self.weight_deacy = 3e-5
