from importlib import import_module
import pandas as pd
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader, random_split
from transformers import AutoTokenizer, AutoModel, AdamW, get_linear_schedule_with_warmup
from preprocess_user import *
from preprocess_job import *
import random
import torch
import faiss
from sentence_transformers import SentenceTransformer, util



def get_features(config, model, sentences, batch=32):
    """

    :param config:
    :param sentences: list
    :param batch:
    :return:
    """
    features = []
    for idx in range(0, len(sentences),batch):

        inputs = config.tokenizer(sentences[idx: idx+batch], return_tensors="pt", padding=True, truncation=True)
        inputs = {k: v.to(config.device) for k, v in inputs.items()}
        with torch.no_grad():
            outputs = model(**inputs)

        features.extend((outputs.pooler_output.cpu().detach().numpy()))
    return features

# def recommned(user_profiles, faiss_idx, job_des, top_k=5):
#     '''
#     for idx, user in enumerate(user_profiles):
#         if idx > 2:
#             break
#         else:
#             user_feature = get_features(config, model, [user])[0]
#             user_feature = np.array(user_feature).astype('float32').reshape((1, -1))
#             _, recommned_idx = faiss_idx.search(user_feature, top_k)
#             recommed_jobs = [job_des[i] for i in recommned_idx[0]]
#
#             print('user profile: {} \n'.format(user))
#             print('recommend jobs: \n')
#             for idx, recommed_job in enumerate(recommed_jobs):
#                 print('recommend {}: {}'.format(idx,recommed_job))
#     '''
#     user_feature = get_features(config, model, [user_profiles])[0]
#     user_feature = np.array(user_feature).astype('float32').reshape((1, -1))
#     _, recommned_idx = faiss_idx.search(user_feature, top_k)
#     recommed_jobs = [job_des[i] for i in recommned_idx[0]]
#     print('user profile: {} \n'.format(user_profiles))
#     print('recommend jobs: \n')
#     for idx, recommed_job in enumerate(recommed_jobs):
#         print('\n')
#         print('recommend {}: {}'.format(idx, recommed_job))


from sklearn.metrics.pairwise import cosine_similarity
def recommend2(user_profile, job_features, job_des, k=5):
    user_feature = get_features(config, model, [user_profile])[0]
    dot_products = np.dot(job_features, user_feature)
    # similarities = np.exp(dot_products) / 10
    similarities = cosine_similarity([user_feature], job_features)[0]
    top_k = np.argsort(similarities)[-k:][::-1]
    recommed_jobs = [(job_des[i], similarities[i]) for i in top_k]

    print('user profile: {} \n'.format(user_profile))
    print('recommend jobs: \n')
    for idx, recommed_job in enumerate(recommed_jobs):
        print('\n')
        print('recommend {}: {}'.format(idx, recommed_job))


def recommend_jobs_sent(resume, job_descriptions, top_k=5):

    resume_embedding = model.encode(resume)
    job_embeddings = model.encode(job_descriptions)


    similarities = util.pytorch_cos_sim(resume_embedding, job_embeddings)

    # top_job_indices = np.argsort(similarities[0])[-top_k:][::-1]
    top_job_recommendations = sorted(zip(job_descriptions, similarities[0].tolist()), key=lambda x: x[1], reverse=True)[
                              :top_k]

    return top_job_recommendations

if __name__ == '__main__':
    x = import_module('models.' + 'siamese')
    config = x.Config()

    # construct dataset
    education, interest, skill = read_csv(config)
    selected_features = combine_features(education, interest, skill)
    cleaned_profiles, cleaned_jobs = preprocess(config)  # list

    # model_name = "cambridgeltl/trans-encoder-cross-simcse-roberta-large"
    # model_name = "YuxinJiang/sup-promcse-roberta-large"
    # tokenizer = AutoTokenizer.from_pretrained(model_name)
    # model = AutoModel.from_pretrained(model_name).to(config.device)
    # extract features
    # job_features = get_features(config, model, cleaned_jobs[:64])
    # job_features = np.array(job_features).astype('float32')
    # faiss inx
    # faiss_idx = faiss.IndexFlatL2(job_features.shape[1])  # query n vectors of dimension d to the index.
    # faiss_idx.add(job_features)
    # recommned(user_profile, faiss_idx, cleaned_jobs)

    # s-bert
    model_name = 'paraphrase-distilroberta-base-v2'
    model = SentenceTransformer(model_name)

    # ml
    # user_profile =  "Data Scientist with 5 years of experience in machine learning, deep learning, and natural language processing. Proficient in Python, TensorFlow, and PyTorch. Experienced in building and deploying machine learning models for various applications including computer vision, speech recognition, and recommendation"
    # art
    # user_profile = "I am a passionate and creative individual with a strong background in art and design. " \
    #                "I have a degree in Fine Arts and have worked as a freelance artist for several years, " \
    #                "creating unique and expressive pieces that have been exhibited in galleries across the country. " \
    #                "I have a keen eye for detail and a deep understanding of color theory, composition, and aesthetics. " \
    #                "I am proficient in a variety of mediums, including painting, sculpture, and digital art, and am always eager to explore new techniques and styles. " \
    #                "I am committed to creating beautiful and meaningful works that inspire and move others."

    # service
    user_profile = "I am a customer service professional with extensive experience in the hospitality industry. I have worked in a variety of roles, including front desk receptionist, restaurant server, and hotel concierge, and have a proven track record of delivering exceptional service and exceeding customer expectations. I am skilled in managing customer inquiries and complaints, handling reservations and bookings, and maintaining a positive and professional demeanor at all times. I am a strong communicator and team player, and am committed to providing an outstanding customer experience."

    # seller
    # user_profile = "I am a results-driven sales professional with a proven track record of exceeding sales targets and driving revenue growth. I have worked in a variety of industries, including technology, retail, and finance, and have a deep understanding of sales techniques and strategies. I am skilled in identifying customer needs and developing customized solutions to meet their unique requirements. I am a persuasive communicator and adept at building strong relationships with customers and colleagues. I am committed to achieving sales success and contributing to the overall growth and success of my organization."
    recommended_jobs = recommend_jobs_sent(user_profile, cleaned_jobs)
    for job, similarity in recommended_jobs:
        print(f"job description: {job}\nsimilarity: {similarity}\n")
    # user profile
    # job desc
    # label: the similarity of t



