# Grow Right
[![growright](./logo.png)](https://www.growright.digital/)


## Project Overview
Grow Right is a revolutionary new job search information service platform that matches job searchers based on their talents and preferences. Job seekers only need to build a profile on the Grow Right platform, which includes a CV, wage expectations, and career goals, to receive job listings for relevant employment.

In terms of technological implementation, Grow Right members filter job applicants' information by keywords to match the best candidates with open positions using machine learning technology and natural language processing models.


## Project Purpose
Our work this semester is mainly divided into three parts: front-end page design, back-end architecture construction and algorithm design.


## Project Board
 + [Trello](https://trello.com/b/VA41WSuO/grow-right)

## Team Chart
| Name        | UID      | Role                | Part              |
|-------------|----------|---------------------|-------------------|
| Bryce  | -        | Client              | -                 |
| Xiaoxiang Kong    |u6828507   | Spokesperson/Member | Frontend, Backend |
| Yi Lin  | u7348307   | Spokesperson/Member | Algorithm         |
| Mengfan Yan  | u7375900  | Member              | Algorithm         |
| Dingyong Liu    | u6540429  | Member              | Frontend          |
| Zixin Su  | u7425489  | Member              | Backend           |
| Kiera Wu   | u7168685  | Member              | Backend           |
| Jizhou Ren       |   u7270477       |   Member| Algorithm         |

##Local Development
   ###Frontend
```bash
cd frontend
```
```bash
yarn install
yarn dev
```

## Task Management
- [User Story Map](https://miro.com/app/board/uXjVMRUaqyg=/)
- [Risk Log](https://docs.google.com/spreadsheets/d/1zlV_5fegWap5s_EIdeSIqMvfWplfZ_agHyAr8Mz7VkY/edit#gid=0)
- [Decision Log](https://docs.google.com/spreadsheets/d/1XpwFNTAbX_2byRMNyapzh000fuHt6z5HPRLkvweDe2g/edit?usp=sharing)
- [Reflection Log](https://docs.google.com/document/d/1ixBI4PYm_ebK9hnG_BKnBpq8cpoTp7IA/edit?usp=share_link&ouid=113528990410475067862&rtpof=true&sd=true)
- [Workload Track](https://docs.google.com/spreadsheets/d/1-e1I2m4_4WSQ0AMttn8JbbcodHlEMNN2/edit#gid=250456221)

##   Conflict Management
```mermaid
graph TD
    A[Acknowledge Conflict] --> B[Encourage Open Communication]
    B --> C[Identify Root Cause]
    B --> D[Listen Actively]
    B --> E[Maintain Neutrality]
    B --> F[Collaborate on Solutions]
    F --> G[Reach Mutual Agreement]
    B --> H[Establish Clear Expectations]
    B --> I[Implement Agreed-Upon Solution]
    I --> J[Monitor Progress & Adjust]
    B --> K[Follow-up]
    K --> L[Ensure Successful Resolution]
    B --> M[Learn from Experience]
```

##  Audit 1
- [Audit 1 slides](https://docs.google.com/presentation/d/1X6_5mr0Oo1V-cqkYVkcW8afICOJrjViY/edit#slide=id.p1)
- [Statement of Work](https://docs.google.com/document/d/1FomSg6jv6JzOz2cx6QxN7vjCUk7zghba/edit?usp=share_link&ouid=113528990410475067862&rtpof=true&sd=true)
- [Constraint](https://docs.google.com/document/d/11xvZBOr4-F22QlFrDZ8b1JK8gq4tCUDNMgPTsCrnZTA/edit?usp=share_link)
- [Potential Cost](https://docs.google.com/document/d/1UTJR9OpjCSe11VEfr9uuTvslJXdi-51F385mT2K1BpA/edit?usp=share_link)
- [Project Client Map](https://docs.google.com/document/d/1qT96FfxsBl9zjE0k0p6z5eD3MSI2Et7W/edit?usp=share_link&ouid=113528990410475067862&rtpof=true&sd=true)
- [Research on the Recommendation System](https://docs.google.com/document/d/1IExQEuH9F9RNl4THinotXcSuI8OlEAWYguYZVvhCYLs/edit)

##  Audit 2

##  Audit 3


##  Meeting Minutes
### Tutorial Meeting
+ https://anu.zoom.us/j/83643237790?pwd=aWdYK3FaTXgxak00ejl3b0k3emlHZz09
+ https://anu.zoom.us/j/89639010562?pwd=R01KeEs3VzNEUkJNS2ROK3N5SmYvQT09

### Team Meeting
- WeChat

- [Week 2 Team Meeting](https://docs.google.com/document/d/1iJVQ2EI_EjoQWuyWJw5Ix95h1NQkQxa5/edit)
- [14/03 Team Meeting](https://docs.google.com/document/d/1uMIFcWYjNndVvQEVsIteGPprmN-ENvFc/edit)
- [24/03 Team Meeting](https://drive.google.com/drive/folders/1MpIgvBBeqZuzGajhcpyid7skKlDQPcRn)
- [18/04 Team Meeting](https://drive.google.com/drive/folders/1sfRunN7TT-QOyGVMpjYW02FkzQdSdDJu)
- [29/04 Team Meeting](https://drive.google.com/drive/folders/1sBidLsu0Y7TYIVZqAuKZWe5wecjNyMcv)
### Client Meeting
https://anu.zoom.us/j/88323004338?pwd=cEQzU05BMmtac0xVQUFHeUlhVm5Xdz09

- [Week 2 Client Meeting](https://docs.google.com/document/d/1qS53j8PGWrn6WL20BCiq-jVBPe4M9Z7F/edit)
- [14/03 Client Meeting](https://docs.google.com/document/d/1CofWnQghxZMC7WXXVAZrTzNetyaJSZOj/edit)
- [24/03 Client Meeting](https://docs.google.com/document/d/1ANXS4f91fD0PmATkdqQDsuaswVI9xol9Jmomc-38Odc/edit)
- [20/04 client meeting] (https://docs.google.com/document/d/1QLFLniR2mrgz9ythXxBOAI7BhI5_4kh5ELWwot-TbTM/edit)
- [05/05 Client Meeting]()
##  Milestone & Deliverables
![Milestone](milestone.drawio.png)
1. Implement the recommendation algorithm
2. Build an interactive web page

##  Repository
[GitLab](https://gitlab.com/Jizhou_REN/grow_right)

[Google Drive](https://drive.google.com/drive/folders/1X5hNd0USqD7AcLkqwQ8mBgZphJhts4GO?usp=share_link)

