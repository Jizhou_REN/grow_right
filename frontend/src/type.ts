export type Position = {
    id: string;
    name: string;
    industry: string;
    publisher: Employer;
    location: string;
    description: string;
    tags: string[];
    maximumSalary?: number;
    minimumSalary?: number;
    interest?: boolean;
}

export type Employer = {
    id: string;
    name: string;
}

export type jobseeker = {
    id: string;
    name: string;
}