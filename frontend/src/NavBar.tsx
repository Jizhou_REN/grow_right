import './App.css'

import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import GrowRightLogo from './assets/GrowRight-logo.png';
//import { useAuth } from './utils/AuthProvider';


function NavBar() {
    //const { user, signout } = useAuth();
    return (
            // <Stack
            //     direction="horizontal"
            //     gap={3}
            //     style={{ width: '100%', padding: '0 5% 0 0' }}
            // >
        <Navbar bg="secondary">
            <Container>
                <Navbar.Brand href="#home">
                    <img
                        alt=""
                        src={GrowRightLogo}
                        width="100%"
                        height="100%"
                        className="d-inline-block align-top"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="#home" id="top-items">Home</Nav.Link>
                        <Nav.Link href="#link">Link</Nav.Link>
                        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">
                                Another action
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">
                                Separated link
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>



            //     {/*{user !== null && (*/}
            //     {/*    <NavDropdown title={user.username} className="bg-light ms-auto">*/}
            //     {/*        <NavDropdown.Item onClick={signout}>Logout</NavDropdown.Item>*/}
            //     {/*    </NavDropdown>*/}
            //     {/*)}*/}
            //
            // {/*</Stack>*/}

    );
}
export default NavBar;
