package com.anu.service;

import com.anu.dto.UserDTO;
import com.anu.entity.User;
import com.anu.exception.UsernameOccupiedException;
import com.anu.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@SpringBootTest
@RunWith(PowerMockRunner.class)
public class LoginServiceTest {

    @InjectMocks
    LoginService loginServiceMock;

    @MockBean
    LoginService loginService;

    @MockBean
    UserMapper userMapper;

    @Test
    public void isUsernameOccupied() {
        // 1. not occupied
        LoginService spyService = Mockito.spy(loginServiceMock);
        PowerMockito.doReturn(false).when(spyService).isUsernameOccupied(Mockito.anyString());
        String testName = "ES3";
        Assert.assertFalse(spyService.isUsernameOccupied(testName));

        // 2. occupied
        LoginService spyService2 = Mockito.spy(loginServiceMock);
        PowerMockito.doReturn(true).when(spyService2).isUsernameOccupied(Mockito.anyString());
        testName = "root"; // must exist
        Assert.assertTrue(spyService2.isUsernameOccupied(testName));
    }

    @Test
    @Transactional
    public void register() {
        // 1. occupied
        LoginService spyService = Mockito.spy(loginServiceMock);
        PowerMockito.doReturn(true).when(spyService).isUsernameOccupied(Mockito.anyString());
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("Lin!1");
        userDTO.setPassword("asdinasdk@");
        try {
            spyService.register(userDTO);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof UsernameOccupiedException);
        }
    }
}