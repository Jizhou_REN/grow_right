package com.anu.exception;

public enum ExceptionCodeEnum {
    USERNAME_OCCUPIED_EXCEPTION(10000, "Username has been occupied");

    private int code;
    private String msg;

    ExceptionCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
