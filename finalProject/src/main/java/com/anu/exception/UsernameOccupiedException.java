package com.anu.exception;

public class UsernameOccupiedException extends RuntimeException{
    public UsernameOccupiedException() {
        super("Username is occupied");
    }
}
