package com.anu.dto;

import lombok.Data;

@Data
public class UserDTO {

    private String username;

    private String password;

    private String webSite;

    private String bio;

    private String profilePhoto;
}
