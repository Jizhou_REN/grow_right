package com.anu.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class UserVO {
    @NotEmpty(message = "Username is missing")
    @Length(min=5, max= 20, message = "User name must be limited")
    private String username;

    @NotEmpty(message = "Password is missing")
    @Length(min = 6, max = 18, message = "Password must be limited")
    private String password;

    private String webSite;

    private String bio;

    private String profilePhoto;
}
