package com.anu.service;

import com.anu.dto.UserDTO;
import com.anu.entity.User;
import com.anu.exception.UsernameOccupiedException;
import com.anu.mapper.UserMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoginService extends ServiceImpl<UserMapper, User> {

    public Boolean isUsernameOccupied(String username) {
        Long count = this.baseMapper.selectCount(new QueryWrapper<User>().eq("username", username));
        return count > 0;
    }

    private void checkUsernameStatus(String username) {
        if (isUsernameOccupied(username)) {
            throw new UsernameOccupiedException();
        }
    }

    public void register(UserDTO dto) {

        // check whether username is occupied
        checkUsernameStatus(dto.getUsername());

        // copy
        User user = new User();
        BeanUtils.copyProperties(dto, user);

        // MD5 encode
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        // to match, use passwordEncoder.matches(password, passwordDB)
        String passwordCode = passwordEncoder.encode(dto.getPassword());

        user.setPassword(passwordCode);

        baseMapper.insert(user);
    }

    public UserDTO login(String username, String password) {
        // require the user
        User user = this.baseMapper.selectOne(new QueryWrapper<User>().eq("username", username));
        if (user == null) {
            return null;
        } else {
            // check the password
            String encodedPassword = user.getPassword();
            // encode
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (encoder.matches(password, encodedPassword)) {
                UserDTO userDTO = new UserDTO();
                BeanUtils.copyProperties(user, userDTO);
                return userDTO;
            }
            return null;
        }
    }

}
