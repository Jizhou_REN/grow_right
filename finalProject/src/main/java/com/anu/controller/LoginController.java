package com.anu.controller;

import com.anu.dto.UserDTO;
import com.anu.exception.ExceptionCodeEnum;
import com.anu.exception.UsernameOccupiedException;
import com.anu.service.LoginService;
import com.anu.utils.R;
import com.anu.vo.UserVO;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/login")
public class LoginController {

    private static final Logger log = Logger.getLogger(LoginController.class);

    private static final String LOGIN_USER = "loginUser";

    @Resource
    private LoginService loginService;

    @PostMapping("/signup")
    public R signUp(@Valid @RequestBody UserVO vo, BindingResult validResult) {
        if (validResult.hasErrors()) {
            validResult.getFieldErrors().forEach(error -> {
                log.error("error filed is: " + error.getField() + " message is: " + error.getDefaultMessage());
            });
            return R.error("Fail to sign up");
        }
        // register
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(vo, userDTO);
        try {
            loginService.register(userDTO);
        } catch (UsernameOccupiedException exception) {
            log.error("error code: " + ExceptionCodeEnum.USERNAME_OCCUPIED_EXCEPTION.getCode()
                    + " message is: " + ExceptionCodeEnum.USERNAME_OCCUPIED_EXCEPTION.getMsg());

            return R.error(ExceptionCodeEnum.USERNAME_OCCUPIED_EXCEPTION.getCode(),
                    ExceptionCodeEnum.USERNAME_OCCUPIED_EXCEPTION.getMsg());
        }
        return R.ok();
    }

    @GetMapping("/check/username")
    public R checkUsername(String username) {
        Map<String, Object> resultMap = new HashMap<>();
        Boolean isOccupied = loginService.isUsernameOccupied(username);
        resultMap.put("result", isOccupied);
        return R.ok(resultMap);
    }

    @PostMapping("/login")
    public R login(UserVO vo, HttpSession session) {
        // service login
        UserDTO user = loginService.login(vo.getUsername(), vo.getPassword());
        if (user == null) {
            return R.error("No valid account");
        }
        session.setAttribute(LOGIN_USER, user);
        return R.ok();
    }

}
