CREATE DATABASE `resume_project`;

USE `resume_project`;

CREATE TABLE IF NOT EXISTS `user_info` (
    `id` LONG NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `username` VARCHAR(50) NOT NULL UNIQUE KEY,
    `password` VARCHAR(100) NOT NULL,
    `website` VARCHAR(100),
    `bio` VARCHAR(120),
    `profile_photo` VARCHAR(255)
)ENGINE=INNODB;